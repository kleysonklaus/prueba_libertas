import 'package:flutter/material.dart';
import 'package:libertas6/Helper/RequestHelper.dart';
import 'package:libertas6/Helper/WidgetHelper.dart';

import '../Constant.dart';

class ElPeruanoDetalle extends StatefulWidget
{
    final String id;

    const ElPeruanoDetalle({Key key, this.id}) : super(key: key);

    @override
    State<StatefulWidget> createState() { return new _ElPeruanoDetalleState(); }
}

class _ElPeruanoDetalleState extends State<ElPeruanoDetalle>
{
    Map<String, dynamic> _post;

    @override
    void initState()
    {
        super.initState();
        this._setPosts();
    }

    void _setPosts() async
    {
        RequestHelper.getObjectFromAPI('/get_elperuanoleycontenido/' + this.widget.id).then((response)
        {
            setState(() { this._post = response; });
        });
    }

    @override
    Widget build(BuildContext context)
    {
        List<Widget> widgets = 
        [
            this._getContent()
        ];
        return WidgetHelper.getStaticContainer(widgets);
    }

    Widget _getContent()
    {
        return Expanded
        (
            child: this._post == null ? WidgetHelper.getWidgetLoader() : 
            Container
            (
                margin: EdgeInsets.only(top: Constant.containerMarginTop, bottom: Constant.containerMarginBottom, left: Constant.containerMarginLeft, right: Constant.containerMarginRight), 
                child: SingleChildScrollView
                (
                    child: new Column
                    (
                        children: 
                        [
                            this._getItemTitle(this._post['titulo']), 
                            this._getItemSubTitle(this._post['subtitulo']), 
                            this._getItemContent(this._post['contenido'])
                        ]
                    )
                )
            )
        );
    }

    Widget _getItemTitle(title)
    {
        return new Container
        (
            margin: EdgeInsets.only(bottom: 10), 
            width: double.infinity, 
            child: new Text
            (
                title, 
                textAlign: TextAlign.center,
                style: new TextStyle
                (
                    fontWeight: FontWeight.bold, 
                    fontSize: 17,
                    color: Constant.primaryColor, 
                )
            )
        );
    }

    Widget _getItemSubTitle(subtitle)
    {
        return new Container
        (
            margin: EdgeInsets.only(bottom: 12), 
            width: double.infinity, 
            child: new Text
            (
                subtitle, 
                textAlign: TextAlign.center,
                style: new TextStyle
                (
                    fontSize: 15, 
                )
            )
        );
    }

    Widget _getItemContent(content)
    {
        return new Container
        (
            child: new Text
            (
                content, 
                textAlign: TextAlign.justify,
                style: new TextStyle
                (
                    fontSize: 14, 
                )
            )
        );
    }
}