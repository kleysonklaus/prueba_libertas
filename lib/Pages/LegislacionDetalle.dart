import 'package:flutter/material.dart';
import 'package:libertas6/Helper/RequestHelper.dart';
import 'package:libertas6/Helper/WidgetHelper.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

import 'package:configurable_expansion_tile/configurable_expansion_tile.dart';

import '../Constant.dart';
import 'Titulo.dart';

class LegislacionDetalle extends StatefulWidget {
  final String url;

  const LegislacionDetalle({Key key, this.url}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return new _LegislacionDetalleState();
  }
}

class _LegislacionDetalleState extends State<LegislacionDetalle>
    with TickerProviderStateMixin {
  List<dynamic> _posts;
  TabController _tabController;
  double cp = 5;

  @override
  Widget build(BuildContext context) {
    List<Widget> widgets = [this._getTabs(), this._getTabContent()];
    // return WidgetHelper.getStaticContainer(widgets);

    return new Scaffold(
        appBar: new AppBar(
          title: new Image.asset(
            'assets/images/logo.jpg',
            width: 120.0,
            //height: 240.0,
            fit: BoxFit.cover,
          ),
          backgroundColor: Constant.primaryColor,
          automaticallyImplyLeading: false,
          actions: <Widget>[
            PopupMenuButton<String>(
              //onSelected: handleClick,
              icon: new Icon(Icons.menu),
              itemBuilder: (BuildContext context) {
                return {'Logout', 'Settings'}.map((String choice) {
                  return PopupMenuItem<String>(
                    value: choice,
                    child: Text(choice),
                  );
                }).toList();
              },
            ),
          ],
        ),
        body: WidgetHelper.getStaticContainer(widgets));
  }

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(length: 2, vsync: this);
    this._setPosts();
  }

  void _setPosts() async {
    RequestHelper.getListFromAPI(this.widget.url).then((response) {
      setState(() {
        this._posts = response;
      });
    });
  }

  /* Widgets */
  Widget _getTabs() {
    return new Container(
        child: new TabBar(
            controller: this._tabController,
            indicatorSize: TabBarIndicatorSize.label,
            labelColor: Constant.primaryColor,
            unselectedLabelColor: Constant.disabledColor,
            isScrollable: true,
            onTap: (value) {
              // print(value);
            },
            tabs: [
          new Container(
            width: 60,
            alignment: Alignment.center,
            child: new Text('Indice'),
          ),
          new Tab(text: 'Ir a')
        ]));
  }

  Widget _getTabContent() {
    return new Container(
        child: Expanded(
      child: TabBarView(
        controller: _tabController,
        children: [
          this._getCodigosDetalleLista(),
          Icon(Icons.directions_transit),
        ],
      ),
    ));
  }

  Widget _getCodigosDetalleLista() {
    if (this._posts == null) {
      return WidgetHelper.getWidgetLoader();
    } else {
      return Container(
          margin: EdgeInsets.only(top: 20),
          child: new SingleChildScrollView(
            child: new Column(
                children: this._posts.map((e) {
              // return this._getLegislacionExpansionTile(e, 1);
              return this._getLegislacionExpansionTile(e, 1, cp + 10);
            }).toList()),
          ));
    }
  }

  Widget _getLegislacionExpansionTile(
      Map<String, dynamic> item, int level, double cp) {
    double childrenPadding = 18;
    TextStyle textStyle = new TextStyle(fontSize: (15 - level).toDouble());

    List<Widget> wChildren = new List();
    String childrenKey = 'children_' + level.toString();

    if (item.containsKey(childrenKey)) {
      List children = item[childrenKey];
      children.forEach((element) {
        // wChildren.add(this._getLegislacionExpansionTile(element, level + 1));
        wChildren
            .add(this._getLegislacionExpansionTile(element, level + 1, cp + 3));
      });
    }

    if (wChildren.length == 0) {
      String titulo = item['titulo_' + level.toString()];

      return new Container(
        margin: EdgeInsets.only(
          left: 15,
          top: 5,
          bottom: 10,
        ),
        width: double.infinity,
        child: new InkWell(
          child: new Text(titulo, style: textStyle),
          onTap: () {
            pushNewScreen(
              context,
              screen: Titulo(id: item['id_titulo'], nombre: titulo),
              withNavBar: true, // OPTIONAL VALUE. True by default.
              pageTransitionAnimation: PageTransitionAnimation.cupertino,
            );
          },
        ),
      );
    } else {
      return Container(
        // padding: EdgeInsets.only(top: 15, bottom: 5),
        ////############################
        child: ConfigurableExpansionTile(
          // // ANIMACION LUEGO DE DESGLOSAR EL TITULO, AUN CON ERRORES DESCONOCIDOS

          // headerExpanded: Flexible(
          //   child: Expanded(
          //     child: Container(
          //       padding: new EdgeInsets.only(
          //           left: cp, top: 10, bottom: 10, right: 10),
          //       width: MediaQuery.of(context).size.width * 0.93,
          //       // color: Color.fromRGBO(200, 200, 200, 1),
          //       child: Row(
          //         children: [
          //           Expanded(
          //             child: Text(
          //               item['titulo_' + level.toString()],
          //               // style: textStyle,
          //               style: TextStyle(
          //                   // fontSize: (15 - level).toDouble(),
          //                   color: Colors.blue),
          //             ),
          //           ),
          //           new Icon(Icons.arrow_drop_up, color: Colors.blue),
          //         ],
          //       ), // Text("A Header"),
          //     ),
          //   ),
          // ),
          header: Expanded(
            child: Container(
              padding:
                  new EdgeInsets.only(left: cp, top: 10, bottom: 10, right: 10),
              width: MediaQuery.of(context).size.width * 0.93,
              // color: Color.fromRGBO(200, 200, 200, 1),
              child: Row(
                children: [
                  Expanded(
                    child: Text(
                      item['titulo_' + level.toString()],
                      style: textStyle,
                    ),
                  ),
                  // Expanded(child: Container()),
                  // Icon(Icons.arrow_drop_down)
                  new Icon(Icons.arrow_drop_down),
                ],
              ), // Text("A Header"),
            ),
          ),
          children: [
            Container(
              padding: new EdgeInsets.only(left: cp),
              width: MediaQuery.of(context).size.width * 0.93,
              color: Color.fromRGBO(200, 200, 200, 0.15),
              child: Column(
                children: wChildren,
                // Padding: new EdgeInsets.only(left: childrenPadding),
              ),
            ),
          ],
        ),
        //############################

        // child: ListTileTheme(
        //   contentPadding: EdgeInsets.only(top: 0, left: 15, bottom: 0),
        //   child: new ExpansionTile(
        //     // ListTileTheme
        //     // backgroundColor: Colors.grey,
        //     title: new Text(
        //       item['titulo_' + level.toString()],
        //       style: textStyle,
        //     ),
        //     children: wChildren,
        //     childrenPadding: new EdgeInsets.only(left: childrenPadding),
        //     // tilePadding: EdgeInsets.only(bottom: 0, top: 0),
        //   ),
        // ),
      );
    }
  }
  /* End Widgets */
}
