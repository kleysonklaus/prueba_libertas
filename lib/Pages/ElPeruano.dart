import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:libertas6/Helper/Helper.dart';
import 'package:libertas6/Helper/RequestHelper.dart';
import 'package:libertas6/Helper/WidgetHelper.dart';
import 'package:libertas6/Pages/ElPeruanoDetalle.dart';
import 'package:libertas6/Pages/VisorPDF.dart';
import 'package:libertas6/Pages/VisorPDF2.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

import '../Constant.dart';

// NUEVOS MODULOS IMPORTADOS
import 'Home.dart';

class ElPeruano extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _ElPeruanoState();
  }
}

class _ElPeruanoState extends State<ElPeruano> {
  Map<String, dynamic> _posts;
  DateTime _date;

  @override
  void initState() {
    super.initState();
    this._date = DateTime.now();
    this._setPosts();
  }

  void _setPosts() async {
    String date = this._date.year.toString() +
        '-' +
        this._date.month.toString() +
        '-' +
        this._date.day.toString();
    RequestHelper.getObjectFromAPI('/get_elperuanocontenido/' + date)
        .then((response) {
      setState(() {
        this._posts = response;
      });
    });
  }

  void _resetData() {
    this._posts = null;
    this._setPosts();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> widgets = [
      WidgetHelper.getBuscador('legislacion', context),
      this._getTitle(),
      this._getPosts()
    ];
    return WidgetHelper.getStaticContainer(widgets);
  }

  Widget _getTitle() {
    return Container(
        width: double.infinity,
        child: IntrinsicHeight(
          child: new Row(
            children: [
              new Expanded(
                child: this._getLogoElPeruano(),
              ),
              this._getIconoCalendario(),
              this._getIconoPDF()
            ],
          ),
        ));
  }

  Widget _getLogoElPeruano() {
    return new Container(
      child: Column(
        children: [
          new Container(
              child: new Image(
            image: AssetImage('assets/images/logo_elperuano.jpg'),
          )),
          new Container(
              width: double.infinity,
              margin: EdgeInsets.only(left: 20),
              child: new Text(
                Helper.getDiaSemanaNombre(this._date.weekday - 1) +
                    ', ' +
                    this._date.day.toString() +
                    ' de ' +
                    Helper.getMesNombre(this._date.month - 1).toString() +
                    ' ' +
                    this._date.year.toString(),
                style: new TextStyle(fontSize: 11, fontWeight: FontWeight.bold),
              ))
        ],
      ),
    );
  }

  Widget _getIconoCalendario() {
    return new Container(
        width: 48,
        alignment: Alignment.center,
        child: InkWell(
          child: new Image(
            image: new AssetImage('assets/icons/png/calendario.png'),
            width: 32,
            color: Color.fromRGBO(127, 127, 127, 1),
          ),
          onTap: () => this._selectDate(context),
        ));
  }

  void _selectDate(BuildContext context) async {
    DateTime picked = await showDatePicker(
      context: context,
      initialDate: this._date,
      firstDate: DateTime(2000),
      lastDate: DateTime.now(),
    );

    if (picked != null && picked != this._date) {
      setState(() {
        this._date = picked;
        this._resetData();
      });
    }
  }

  Widget _getIconoPDF() {
    return new Container(
        width: 48,
        alignment: Alignment.center,
        child: this._posts == null
            ? new ImageIcon(
                AssetImage('assets/icons/png/pdf.png'),
                color: Colors.grey,
                size: 32,
              )
            : InkWell(
                child: new Image(
                    image: new AssetImage('assets/icons/png/pdf.png'),
                    width: 32),
                onTap: () {
                  print('=====================================');
                  // hideNavBar();
                  // bottomNavigationBar: true ? new BottomNavigationBar(
                  //   type: BottomNavigationBarType.fixed,
                  //   unselectedItemColor: Colors.grey,
                  //   selectedItemColor: Colors.orange, // new
                  // currentIndex: 3,
                  //   items: [
                  //     new BottomNavigationBarItem(icon: new Icon(Icons.home), title: new Text("Home")),
                  //     new BottomNavigationBarItem(icon: new Icon(Icons.search), title: new Text("Search")),
                  //     new BottomNavigationBarItem(icon: new Icon(Icons.notifications_none), title: new Text("Notifications")),
                  //     new BottomNavigationBarItem(icon: new Icon(Icons.shopping_basket), title: new Text("Bag")),
                  //   ]) : Container();
                    
                  pushNewScreen(
                    context,
                    // screen: VisorPDF(sourceLink: this._posts['pdf']),
                    screen: ShowPdf(pdfLink: this._posts['pdf']),

                    withNavBar: true, // OPTIONAL VALUE. True by default.
                    pageTransitionAnimation: PageTransitionAnimation.cupertino,
                  );
                },
              ));
  }

  Widget _getPosts() {
    return Expanded(
        child: this._posts == null
            ? WidgetHelper.getWidgetLoader()
            : Container(
                child: SingleChildScrollView(
                    child: new Column(
                children: this._posts['data'].map<Widget>((e) {
                  return new Container(
                      margin: EdgeInsets.only(
                          left: Constant.containerMarginLeft,
                          right: Constant.containerMarginRight,
                          top: 10,
                          bottom: Constant.containerMarginBottom),
                      child: new Column(children: [
                        new Container(
                            width: double.infinity,
                            margin: EdgeInsets.only(bottom: 10),
                            child: new Text(e['Titulo'],
                                style: new TextStyle(
                                    // color: Constant.primaryColor,
                                    color: Color.fromRGBO(0, 0, 78, 1),
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18))),
                        new Container(
                            child: new Column(
                                children: e['Leyes'].map<Widget>((f) {
                          return new Column(children: [
                            this._getItemTitle(
                                f['Titulo2'], f['id_ley'], f['pdf']),
                            this._getItemContent(f['Contenido'])
                          ]);
                        }).toList()))
                      ]));
                }).toList(),
              ))));
  }

  Widget _getItemTitle(String title, String id, String pdf) {
    return new Container(
      width: double.infinity,
      margin: EdgeInsets.only(bottom: 3),
      child: new RichText(
        text: new TextSpan(
          children: [
            new TextSpan(
              text: title,
              style: new TextStyle(
                  color: Color.fromRGBO(52, 43, 147, 1) /*Colors.blue*/,
                  fontWeight: FontWeight.bold),
              recognizer: new TapGestureRecognizer()
                ..onTap = () {
                  pushNewScreen(
                    context,
                    screen: ElPeruanoDetalle(id: id),
                    withNavBar: true, // OPTIONAL VALUE. True by default.
                    pageTransitionAnimation: PageTransitionAnimation.cupertino,
                  );
                },
            ),
            WidgetSpan(
              alignment: PlaceholderAlignment.bottom,
              style: new TextStyle(),
              child: new Container(
                margin: EdgeInsets.only(left: 5),
                child: new InkWell(
                  child: new ImageIcon(
                    AssetImage('assets/icons/png/pdf.png'),
                    color: Colors.red,
                    size: 14,
                  ),
                  onTap: () {
                    print(pdf);
                    pushNewScreen(
                      context,
                      // screen: VisorPDF(sourceLink: pdf),
                      screen: ShowPdf(pdfLink: pdf),    //NUEVO VISOT DE PDF
                      withNavBar: true, // OPTIONAL VALUE. True by default.
                      pageTransitionAnimation:
                          PageTransitionAnimation.cupertino,
                    );
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _getItemContent(content) {
    return new Container(
        child: new Text(content, style: new TextStyle(fontSize: 13)));
  }
}
