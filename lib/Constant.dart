import 'package:flutter/material.dart';
import 'package:libertas6/Classes/MyChip.dart';

abstract class Constant {
  static String apiUrl = 'https://libertas.pe/legislacion';

  static double containerMarginLeft = 20;
  static double containerMarginTop = 20;
  static double containerMarginRight = 20;
  static double containerMarginBottom = 20;

  static Color primaryColor = Color(0xFF040466);
  static Color disabledColor = Color(0xFFB7B7B7);
  static Color contentColor = Color(0xFF060605);

  static List<MyChip> _getLegislacionChips() {
    List<MyChip> list = new List();
    list.add(new MyChip('Todos los contenidos', '', true));
    list.add(new MyChip('Constitución Politica del Perú',
        '/get_busquedacontenido/documento/36,36/', false));
    list.add(new MyChip('Código Procesal Constitucional',
        '/get_busquedacontenido/documento/36,36/', false));
    list.add(new MyChip(
        'Código  Civil', '/get_busquedacontenido/documento/36,36/', false));
    list.add(new MyChip('TUO Código Precesal Civil',
        '/get_busquedacontenido/documento/36,36/', false));
    list.add(new MyChip(
        'Código Penal', '/get_busquedacontenido/documento/36,36/', false));
    list.add(new MyChip('Nuevo Código Procesal Penal',
        '/get_busquedacontenido/documento/36,36/', false));
    list.add(new MyChip('Código Procesal Penal',
        '/get_busquedacontenido/documento/36,36/', false));
    list.add(new MyChip('Código de Procedimientos Penales',
        '/get_busquedacontenido/documento/36,36/', false));
    list.add(new MyChip('Código de los Niños y Adolescente',
        '/get_busquedacontenido/documento/36,36/', false));
    list.add(new MyChip('Código de Prot. y Defensa del Consumidor',
        '/get_busquedacontenido/documento/36,36/', false));
    list.add(new MyChip('Ley Procesal del Trabajo',
        '/get_busquedacontenido/documento/36,36/', false));
    return list;
  }

  static List<MyChip> legislacionChips = _getLegislacionChips();
}
