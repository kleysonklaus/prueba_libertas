import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:libertas6/Constant.dart';
import 'package:libertas6/Helper/RequestHelper.dart';
import 'package:libertas6/Helper/WidgetHelper.dart';
import 'package:libertas6/Modals/ConcordanciaModal.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class Titulo extends StatefulWidget
{
    final String id;
    final String nombre;

    const Titulo({Key key, this.id, this.nombre}) : super(key: key);

    @override
    State<StatefulWidget> createState() { return new _TituloState(); }
}

class _TituloState extends State<Titulo>
{
    List<dynamic> _posts;

    /* Delete */
    static List<String> _concordancias = 
    [
        "Resolución Ministerial N° 389-2004/MINSA del 21 de abril de 2004, se precisó que la expedición del Certificado de Nacido Vivo es gratuita en todos los establecimientos de salud del país, públicos y privados", 
        "R.M.Nº 148-2012-MINSA (Aprueban Directiva Administrativa que establece procedimiento para el registro del Certificado de Nacido Vivo en todos los establecimientos de salud del país"
    ];
    /* End Delete */

    @override
    void initState()
    {
        super.initState();
        this._setPosts();
    }

    void _setPosts() async
    {
        RequestHelper.getListFromAPI('/get_titulocontenido/' + this.widget.id).then((response)
        {
            setState(() { this._posts = response; });
        });
    }

    @override
    Widget build(BuildContext context)
    {
        List<Widget> widgets = 
        [
            // WidgetHelper.getBuscador("titulo", context),

            WidgetHelper.getLogoTop(context),

            this._wrap()
        ];
        return WidgetHelper.getStaticContainer(widgets);
    }

    Widget _wrap()
    {
        return Expanded
        (
            child: new Container
            (
                margin: EdgeInsets.only(
                  top: 0,
                  left: Constant.containerMarginLeft,
                  right: Constant.containerMarginRight
                ), 
                child: SingleChildScrollView(
                  child: new Column
                  (
                      children: 
                      [
                          this._getTitulo(), 
                          this._getArticulos()
                      ]
                  ),
                )
            ),
        );
    }

    Widget _getTitulo()
    {
        return new Container
        (
            width: double.infinity,
            margin: EdgeInsets.only(bottom: 15, top: 15,), 
            child: new Text
            (
                this.widget.nombre, 
                style: new TextStyle
                (
                    color: Constant.primaryColor, 
                    fontWeight: FontWeight.bold, 
                    fontSize: 16, 

                ),
                textAlign: TextAlign.center,
            )
        );
    }

    Widget _getArticulos()
    {
        // return new Expanded
        return new Container
        (
            child: this._posts == null ? WidgetHelper.getWidgetLoader() : new Container
            (
                //child: SingleChildScrollView(
                    child: new Column
                    (
                        children: this._posts.map((e)
                        {
                            return new Container
                            (
                                child: new Column
                                (
                                    children: 
                                    [
                                        this._getArticuloTitulo(e['Titulo']), 
                                        this._getArticuloContenido(e['Contenido']), 
                                        new Container
                                        (
                                            margin: EdgeInsets.only(top: 5, bottom: 5), 
                                            width: MediaQuery.of(context).size.width * 0.55,
                                            child: new Divider
                                            (
                                                thickness: 2,
                                            )
                                        )
                                        
                                    ],
                                )
                            );
                        }).toList(),
                    )
                //)
            )
        );
    }

    Widget _getArticuloTitulo(String  titulo)
    {
        return new Container
        (
            width: double.infinity,
            margin: EdgeInsets.only(bottom: 5), 
            child: new Text
            (
                titulo, 
                style: new TextStyle
                (
                    color: Constant.primaryColor, 
                    fontWeight: FontWeight.bold
                ),
            )
        );
    }

    Widget _getArticuloContenido(String  contenido)
    {
        return new Container
        (
            child: SelectableText.rich
            (
                new TextSpan
                (
                    children: 
                    [
                        new TextSpan
                        (
                            text: contenido
                        ), 
                        new TextSpan
                        (
                            text: ' CONCORDANCIA', 
                            style: TextStyle
                            (
                                color: new Color(0xFFA44702), 
                                fontWeight: FontWeight.bold
                            ),
                            recognizer: TapGestureRecognizer()
                            ..onTap = ()
                            {
                                pushDynamicScreen
                                (
                                    context,
                                    screen: ConcordanciaModal(this._getConcordancias(_concordancias)),
                                    withNavBar: false,
                                );
                            },
                        )
                    ]
                ), 
                // toolbarOptions: new ToolbarOptions(copy: true, selectAll: false),
            )
        );
    }

    List<Widget> _getConcordancias(List<String> concordancias)
    {
        return concordancias.map((e)
        {
            return Container
            (
                margin: EdgeInsets.only(bottom: 10), 
                child: IntrinsicHeight
                (
                    child: new Row
                    (
                        children: 
                        [
                            new Container
                            (
                                alignment: Alignment.topCenter, 
                                margin: EdgeInsets.only(right: 20, top: 3), 
                                child: new Icon
                                (
                                    Icons.brightness_1, 
                                    color: Color(0xFFA44702), 
                                    size: 12,
                                ), 
                            ), 
                            new Container
                            (
                                child: Expanded(
                                    child: new Text
                                    (
                                        e, 
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 20,
                                    ),
                                ),
                            )
                        ]
                    ),
                )
            );
        }).toList();
    }
    /* End Widget */
}