import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import '../Constant.dart';

abstract class RequestHelper {
  static String _errorMessage = 'Fallo cargando los datos';

  static Future<List<dynamic>> getListFromConfig(String filename) async {
    final contents = await rootBundle.loadString('assets/contents/' + filename);
    return json.decode(contents);
  }

  static Future<Map<String, dynamic>> getObjectFromAPI(String url) async {
    http.Response response = await http.get(Constant.apiUrl + url);

    // String source = Utf8Decoder().convert(response.bodyBytes);
    String source = latin1.decode(response.bodyBytes);
    if (response.statusCode == 200) {
      print("EL PERUANOOOOOOOOOOOOOOOOOOOOOO");
      // return json.decode(response.body);
      return json.decode(source);
    } else {
      throw new Exception(_errorMessage);
    }
  }

  static Future<List<dynamic>> getListFromAPI(String url) async {
    http.Response response = await http.get(Constant.apiUrl + url);
    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      throw new Exception(_errorMessage);
    }
  }

  static String utf8convert(String text) {
    List<int> bytes = text.toString().codeUnits;
    return utf8.decode(bytes);
  }
}
