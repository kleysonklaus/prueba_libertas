import 'package:flutter/material.dart';
import 'package:libertas6/Classes/MyChip.dart';
import 'package:libertas6/Constant.dart';

class ChipsModal extends ModalRoute<void>
{
    String _tipo;

    ChipsModal(String tipo)
    {
        this._tipo = tipo;
    }

    @override
    Duration get transitionDuration => Duration(milliseconds: 0);

    @override
    bool get opaque => false;

    @override
    bool get barrierDismissible => false;

    @override
    Color get barrierColor => Colors.black.withOpacity(0.5);

    @override
    String get barrierLabel => null;

    @override
    bool get maintainState => true;

    @override
    Widget buildPage(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation)
    {
        // This makes sure that text and other content follows the material style
        return Material
        (
            type: MaterialType.transparency,
            // make sure that the overlay content is not cut off
            child: SafeArea
            (
                child: _buildOverlayContent(context),
            ),
        );
    }

    Widget _buildOverlayContent(BuildContext context)
    {
        return Container
        (
            margin: EdgeInsets.only(left: 20, right: 20, bottom: 30, top: 30),
            padding: EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 10),
            color: Colors.white,
            child: SingleChildScrollView
            (
                child: new Column
                (
                    children: 
                    [
                        Container
                        (
                            child: new Row
                            (
                                children: 
                                [
                                    new Container
                                    (
                                        margin: EdgeInsets.only(right: 10), 
                                        child: IconButton
                                        (
                                            icon: new Icon(Icons.arrow_back), 
                                            onPressed: () { Navigator.pop(context); }, 
                                        )
                                    ), 
                                    new Expanded
                                    (
                                        child: new Text
                                        (
                                            'Declaración Jurada', 
                                            style: TextStyle
                                            (
                                                fontSize: 16,
                                                fontWeight: FontWeight.bold
                                            )
                                        )
                                    ), 
                                    new Container
                                    (
                                        child: IconButton
                                        (
                                            icon: new Icon(Icons.close), 
                                            onPressed: () { Navigator.pop(context); }, 
                                        )
                                    )
                                ],
                            )
                        ), 
                        new MyChipState(tipo: this._tipo)
                    ],
                ),
            )
        );
    }
}

class MyChipState extends StatefulWidget
{
    final String tipo;

    const MyChipState({Key key, this.tipo}) : super(key: key);

    @override
    State<StatefulWidget> createState() { return new _MyChipStateState(); }
}

class _MyChipStateState extends State<MyChipState>
{
    List<MyChip> myChips = new List();

    @override
    void initState()
    {
        super.initState();
        if(this.widget.tipo == "legislacion") { myChips = Constant.legislacionChips; }
        else if(this.widget.tipo == "titulo") { myChips = Constant.legislacionChips; }
    }

    @override
    Widget build(BuildContext context)
    {
        return new Container
        (
            child: new Wrap
            (
                spacing: 10.0,
                runSpacing: 1.0,
                children: myChips.map((entry) => new FilterChip
                (
                    label: new Text
                    (
                        entry.getLabel(), 
                        style: TextStyle
                        (
                            color: entry.isSelected() ? Colors.white : Colors.black
                        ),
                    ), 
                    selected: entry.isSelected(), 
                    selectedColor: Constant.primaryColor,
                    showCheckmark: false,
                    onSelected: (bool selected)
                    {
                        setState(()
                        {
                            entry.setSelected(selected);
                        });
                    }
                )).toList()
            )
        );
    }
}
