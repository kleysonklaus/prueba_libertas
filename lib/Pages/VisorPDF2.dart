import 'package:advance_pdf_viewer/advance_pdf_viewer.dart';

import 'package:flutter/material.dart';

class ShowPdf extends StatefulWidget {
  final String pdfLink;

  const ShowPdf({Key key, this.pdfLink}) : super(key: key);
  @override
  _ShowPdfState createState() => _ShowPdfState();
}

class _ShowPdfState extends State<ShowPdf> {
  bool _isLoading = true;
  PDFDocument document;

  @override
  void initState() {
    super.initState();
    loadDocument();
  }

  loadDocument() async {
    // document = await PDFDocument.fromAsset('assets/sample.pdf');
    document = await PDFDocument.fromURL(this.widget.pdfLink);

    setState(() => _isLoading = false);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Center(
          child: _isLoading
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : PDFViewer(
                  document: document,
                  zoomSteps: 1,
                ),
        ),
      ),
    );
  }
}
