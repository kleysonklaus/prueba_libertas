import 'package:flutter/material.dart';
import 'package:libertas6/Constant.dart';
import 'package:libertas6/Fonts/libertas_icons.dart';
import 'package:libertas6/Helper/RequestHelper.dart';
import 'package:libertas6/Helper/WidgetHelper.dart';

class Jurisprudencia extends StatefulWidget
{
    @override
    State<StatefulWidget> createState() { return new _JurisprudenciaState(); }
}

class _JurisprudenciaState extends State<Jurisprudencia> with TickerProviderStateMixin
{
    List<dynamic> _posts;
    TabController _tabController;

    @override
    Widget build(BuildContext context)
    {
        List<Widget> widgets = 
        [
            WidgetHelper.getBuscador("legislacion", context), 
            this._getTabs(), 
            this._getTabContent()
        ];
        return WidgetHelper.getStaticContainer(widgets);
    }

    @override
    void initState()
    {
        super.initState();
        _tabController = new TabController
        (
            length: 2, 
            vsync: this
        );
        this._setPosts();
    }

    void _setPosts() async
    {
        RequestHelper.getListFromConfig('lista_jurisprudencia.json').then((response)
        {
            setState(() { this._posts = response; });
        });
    }

    Widget _getTabs()
    {
        return new Container
        (
            child: new TabBar
            (
                controller: _tabController,
                indicatorSize: TabBarIndicatorSize.label,
                labelColor: Constant.primaryColor,
                unselectedLabelColor: Constant.disabledColor,
                isScrollable: true,
                onTap: (value)
                {
                    //print(value);
                },
                tabs: 
                [
                    new Tab(text: 'Poder Judicial'), 
                    new Tab(text: 'Tribunal constitucional')
                ]
            )
        );
    }

    Widget _getTabContent()
    {
        return new Container
        (
            child: Expanded
            (
                child: TabBarView
                (
                    controller: _tabController,
                    children: 
                    [
                        this._getCodigosLista(), 
                        Icon(Icons.directions_transit),
                    ],
                ),
            )
        );
    }

    Widget _getCodigosLista()
    {
        if(this._posts == null) { return WidgetHelper.getWidgetLoader(); }
        else
        {
            print(this._posts);
            return new Container
            (
                margin: EdgeInsets.only(top: 20, left: Constant.containerMarginLeft, right: Constant.containerMarginRight), 
                child: new SingleChildScrollView
                (
                    child: new Column
                    (
                        children: this._posts.map<Widget>((e) => this._getWidgetItemContent(e)).toList()
                    ),
                )
            );
        }
    }

    Widget _getWidgetItemContent(Map<String, dynamic> content)
    {
        return new Container
        (
            margin: new EdgeInsets.only(bottom: 20.0), 
            child: new InkWell
            (
                onTap: () 
                {
                    // pushNewScreen
                    // (
                    //     context,
                    //     screen: JurisprudenciaDetalle(url: content["url"]),
                    //     withNavBar: true, // OPTIONAL VALUE. True by default.
                    //     pageTransitionAnimation: PageTransitionAnimation.cupertino,
                    // );
                },
                child: new Row
                (
                    children: 
                    [
                        new Container
                        (
                            margin: EdgeInsets.only(right: 10), 
                            // child: new Icon
                            // (
                            //     Icons.access_alarms, 
                            //     color: Color(0xFFD07912),
                            // )
                            // child: new ImageIcon
                            // (
                            //     AssetImage('assets/icons/png/comparacion.png'), 
                            //     color: Color(0xFFD07912),
                            //     size: 14,
                            // ),
                            child: new Icon
                            (
                                // LibertasIcons.balance_scale, 
                                myIcons[ content['icon'] ],
                                color: Color(0xFFD07912),
                                size: 16
                            )
                        ), 
                        new Text
                        (
                            content['label'], 
                            style: new TextStyle
                            (
                                color: Constant.contentColor, 
                                fontSize: 16
                            ), 
                            textAlign: TextAlign.justify,
                        ),
                    ]
                ),
            )
        );
    }

    
}