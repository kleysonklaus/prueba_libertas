import 'package:flutter/material.dart';
import 'package:libertas6/Modals/ChipsModal.dart';
import 'package:libertas6/Pages/LegislacionResultados.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

import '../Constant.dart';

abstract class WidgetHelper
{
    static Widget _getMain(Widget container)
    {
        return Scaffold
        (
            body: new SafeArea
            (
                child: new Container
                (
                    padding: new EdgeInsets.all(0),
                    child: container
                ), 
            )
        );
    }

    static Widget getLogoTop(BuildContext context){
      return Container(
        height: 50,
        width: MediaQuery.of(context).size.width * 1.0,
        color: Color.fromRGBO(4, 20, 79, 1),
        alignment: Alignment.center,
        child: new Image.asset(
          'assets/images/logo.jpg', 
          width: 120.0,
        )
      );
    }

    static Widget getStaticContainer(List<Widget> elements)
    {
        return _getMain(new Column(children: elements));
    }

    static Widget getDynamicContainer(List<Widget> elements)
    {
        return _getMain(new SingleChildScrollView
        (
            child: new Column(children: elements,)
        ));

        //return _getMain(new ListView(children: elements,));
    }

    static Widget getWidgetLoader()
    {
        return new Container
        (
            alignment: Alignment.center,
            child: new CircularProgressIndicator()
        );
    }

    /* Dialog */
    static void showAlert(BuildContext context, Widget title, Widget content)
    {
        AlertDialog dialog = new AlertDialog
        (
            title: title,
            content: content,
        );
        _showDialog(context, dialog);
    }

    static void _showDialog(context, dialog)
    {
        showDialog
        (
            context: context, 
            builder: (context)  { return dialog; },
        );
    }
    /* End Dialog */

    /* Custom */
    static Widget getBuscador(String typeOfChip, BuildContext context)
    {
        TextEditingController controller = new TextEditingController();

        return new Container
        (
            color: Color(0xFFF4F3FB),
            margin: EdgeInsets.only
            (
                left: Constant.containerMarginLeft, 
                right: Constant.containerMarginRight
            ),
            child: new Row
            (
                children: 
                [
                    new Container
                    (
                        margin: EdgeInsets.only(right: 10),
                        child: IconButton
                        (
                            icon: new Icon(Icons.search), 
                            onPressed: ()
                            {
                                //print(controller.value.text);
                                pushNewScreen
                                (
                                    context,
                                    screen: LegislacionResultados(),
                                    withNavBar: true, // OPTIONAL VALUE. True by default.
                                    pageTransitionAnimation: PageTransitionAnimation.cupertino,
                                );
                            },
                        )
                    ), 
                    new Expanded
                    (
                        child: new TextField
                        (
                            controller: controller,
                            decoration: new InputDecoration
                            (
                                border: InputBorder.none,
                                focusedBorder: InputBorder.none,
                                enabledBorder: InputBorder.none,
                                errorBorder: InputBorder.none,
                                disabledBorder: InputBorder.none,
                                hintText: 'Buscar'
                            ),
                        )
                    ), 
                    new Container
                    (
                        margin: EdgeInsets.only(left: 10),
                        child: new IconButton
                        (
                            icon: new Icon(Icons.dehaze, color: Constant.primaryColor), 
                            onPressed: () 
                            {
                                pushDynamicScreen
                                (
                                    context,
                                    screen: ChipsModal("legislacion"),
                                    withNavBar: false,
                                );
                            },
                        )
                    )
                ],
            )
        );
    }
}