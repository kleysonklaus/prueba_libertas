import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:libertas6/Classes/MyChip.dart';
import 'package:libertas6/Helper/RequestHelper.dart';
import 'package:libertas6/Helper/WidgetHelper.dart';

import '../Constant.dart';

class LegislacionResultados extends StatefulWidget
{
    final String clave;

    const LegislacionResultados({Key key, this.clave}) : super(key: key);

    @override
    State<StatefulWidget> createState() { return new _LegislacionResultadosState(); }
}

class _LegislacionResultadosState extends State<LegislacionResultados> with TickerProviderStateMixin
{
    List<MyChip> _chips;
    HashMap<String, List<dynamic>> _posts;
    TabController _tabController;

    @override
    void initState()
    {
        super.initState();
        this._posts = new HashMap();
        this._posts['Todos los contenidos'] = new List<dynamic>();
        this._chips = Constant.legislacionChips.where((element) => element.isSelected()).toList();
        _tabController = new TabController
        (
            length: this._chips.length, 
            vsync: this
        );
        this._setPosts();
    }

    void _setPosts() async
    {
        Constant.legislacionChips.forEach((element)
        {
            if(element.isSelected() && element.getUrl() != '')
            {
                RequestHelper.getListFromAPI(element.getUrl()).then((response)
                {
                    setState(()
                    {
                        this._posts[element.getLabel()] = response;
                        response.forEach((e) { e['etiqueta'] = element.getLabel(); });
                        this._posts['Todos los contenidos'].addAll(response);
                    });
                });
            }
        });
    }

    @override
    Widget build(BuildContext context)
    {
        List<Widget> widgets = 
        [
            this._getTabs(), 
            this._getTabContent()
        ];
        // return WidgetHelper.getStaticContainer(widgets);

        return new Scaffold
        (
            appBar: new AppBar
            (
                title: new Image.asset
                (
                    'assets/images/logo.jpg', 
                    width: 120.0,
                    //height: 240.0,
                    fit: BoxFit.cover,
                ), 
                backgroundColor: Constant.primaryColor, 
                automaticallyImplyLeading: false, 
                actions: <Widget>
                [
                    PopupMenuButton<String>
                    (
                        //onSelected: handleClick,
                        icon: new Icon(Icons.menu),
                        itemBuilder: (BuildContext context)
                        {
                            return {'Logout', 'Settings'}.map((String choice)
                            {
                                return PopupMenuItem<String>(
                                value: choice,
                                child: Text(choice),
                                );
                            }).toList();
                        },
                    ),
                ],
            ),
            body: WidgetHelper.getStaticContainer(widgets)
        );
    }

    /* Widgets */
    Widget _getTabs()
    {
        return new Container
        (
            child: new TabBar
            (
                controller: this._tabController,
                indicatorSize: TabBarIndicatorSize.label,
                labelColor: Constant.primaryColor,
                unselectedLabelColor: Constant.disabledColor,
                isScrollable: true,
                onTap: (value)
                {
                    // print(value);
                },
                tabs: this._chips.map((e) => new Tab(text: e.getLabel())).toList()
            )
        );
    }

    Widget _getTabContent()
    {
        return new Container
        (
            child: Expanded
            (
                child: TabBarView
                (
                    controller: _tabController, 
                    children: this._chips.map((e) => this._getTabResults(e.getLabel())).toList()
                ),
            )
        );
    }

    Widget _getTabResults(String label)
    {
        if(!this._posts.containsKey(label)) { return WidgetHelper.getWidgetLoader(); }
        else
        {
            List<dynamic> posts = this._posts[label];
            return new Container
            (
                margin: EdgeInsets.only(top: Constant.containerMarginTop, right: Constant.containerMarginRight, bottom: Constant.containerMarginBottom, left: Constant.containerMarginLeft), 
                child: new Column
                (
                    children: 
                    [
                        new Container
                        (
                            width: double.infinity,
                            margin: EdgeInsets.only(bottom: 15), 
                            child: new Text
                            (
                                'Se encontraron ' + posts.length.toString() + ' resultados', 
                                style: new TextStyle
                                (
                                    color: Colors.grey, 
                                    fontStyle: FontStyle.italic
                                ),
                            )
                        ), 
                        new Expanded
                        (
                            child: new SingleChildScrollView
                            (
                                child: new Column
                                (
                                    children: posts.map((e) => this._getItem(label, e)).toList()
                                ),
                            ),
                        )
                    ]
                )
            );
        }
        
    }

    Widget _getItem(String label, post)
    {
        return new Container
        (
            child: new Column
            (
                children: 
                [
                    label == 'Todos los contenidos' ? 
                    new Container
                    (
                        width: double.infinity, 
                        margin: EdgeInsets.only(bottom: 3), 
                        child: new Text
                        (
                            post['etiqueta'], 
                            style: TextStyle
                            (
                                
                            )
                        )
                    )
                    :
                    new Container(), 
                    new Container
                    (
                        width: double.infinity,
                        margin: EdgeInsets.only(bottom: 5), 
                        child: new Text
                        (
                            post['Titulo'], 
                            style: new TextStyle
                            (
                                color: Constant.primaryColor, 
                                fontWeight: FontWeight.bold
                            ),
                        )
                    ), 
                    new Container
                    (
                        width: double.infinity,
                        child: new Text
                        (
                            post['Decreto_legislativo'], 
                            style: new TextStyle
                            (
                                color: Colors.grey, 
                                fontSize: 11
                            )
                        )
                    ), 
                    new Container
                    (
                        child: new Divider
                        (
                            thickness: 1,
                        ),
                    )
                ],
            )
        );
    }
    /* End Widgets */
}